package test;


import java.util.Iterator;

import model.data_estructuras.EncadenamientoSeparadoTH;
import junit.framework.TestCase;

public class PruebaEncadenamientoSeparado extends TestCase
{
	GeneradorDatos generador = new GeneradorDatos();
	EncadenamientoSeparadoTH<String,Integer > tabla ;
	
	private void setupEscenario1()
	{
		tabla = new EncadenamientoSeparadoTH<>(10);
	}
	
	private void setupEscenario2( int cd){
		setupEscenario1();
		int cantidad =cd ; 
		try{
			String titulos[]=generador.generarCadenas(cantidad,5);
			int agnos[]= generador.generarNumeros(cantidad);
			
			for (int i = 0; i < agnos.length; i++) {
				tabla.insertar(titulos[i],agnos[i] );
			}
		}catch(Exception e){
			fail("No debio sacar excepcion ");
		}
	}
	
	
	public void testAgregar500(){
		setupEscenario2(500);
		tabla.darValor("aaa");
	}
	
	public void testAgregar1000(){
		setupEscenario2(1000);
		tabla.darValor("aaa");
	}
	
	public void testAgregar1500(){
		setupEscenario2(1500);
		tabla.darValor("aaa");
	}
	
	public void testAgregar5000(){
		setupEscenario2(5000);
		int cd=0;
		int[]a=tabla.darLongitudListas();
		for (int i = 0; i < a.length; i++) {
			cd+=a[i];
			System.out.println((i+1)+"//"+a[i]);
		}
		System.out.println("total: "+cd);
		tabla.darValor("aaa");
	}
	
	public void testAgregar10000(){
		setupEscenario2(10000);
		tabla.darValor("aaa");
	}
	
	public void testAgregar50000(){
		setupEscenario2(50000);
	}
	
	public void testpruebaVacia(){
		try{
			setupEscenario1();
			tabla.darValor("aaa");
			fail("debio lanzar una excepcion ");
		}catch(Exception e){

		}
	}
	
	




	
}
