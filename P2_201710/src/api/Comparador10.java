package api;

import java.util.Comparator;

import model.vo.VOPelicula;

public class Comparador10 implements Comparator<VOPelicula>{

	@Override
	public int compare(VOPelicula o1, VOPelicula o2) {
		
		if(o1.getPromedioAnualVotos()*o1.getRatingIMBD() == o2.getPromedioAnualVotos()*o2.getRatingIMBD())
			return 0;
		else if(o1.getPromedioAnualVotos()*o1.getRatingIMBD() < o2.getPromedioAnualVotos()*o2.getRatingIMBD())
			return -1;
		else
			return 1;
	}
}
