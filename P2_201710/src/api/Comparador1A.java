package api;

import java.util.Comparator;

import model.vo.VOUsuarioEntrada;

public class Comparador1A implements Comparator<VOUsuarioEntrada> {

	
	public int compare(VOUsuarioEntrada o1, VOUsuarioEntrada o2) {
		if(o1.getUsuarioName() !=null){
			return -1;
		}
		else if (o1.getUsu() !=null){
			if(o1.getUsu().getTimeS().before( o2.getUsu().getTimeS())){
				return 1;
			}
			else  if(o1.getUsu().getTimeS().equals(o2.getUsu().getTimeS())){
				if(o1.getUsu().getNose().darNumeroElementos() > o2.getUsu().getNose().darNumeroElementos()){
					return 1;
				}
				else if(o1.getUsu().getNose().darNumeroElementos() == o2.getUsu().getNose().darNumeroElementos()){
					return (o1.getUsu().getIdUsuario() - o2.getUsu().getIdUsuario());
				}
				else
					return -1;
			}
			else
				return -1;

		}
		else
			return -1;
	}

	
}
