package api;

import java.util.Comparator;

import model.vo.VOPelicula;

public class Comparador11 implements Comparator<VOPelicula> {

	@Override
	public int compare(VOPelicula o1, VOPelicula o2) {
		
		if(o1.getYear() > o2.getYear())
		{
			return 1;
		}
		else if(o1.getYear() < o2.getYear())
		{
			return -1;
		}
		else
		{
			if(o1.getPaisito().compareToIgnoreCase(o2.getPaisito())>0)
			{
				return 1;
			}
			else if(o1.getPaisito().compareToIgnoreCase(o2.getPaisito())<0)
			{
				return -1;
			}
			else
			{
				if(o1.getGenerosAsociados().darElemento(0).compareTo(o2.getGenerosAsociados().darElemento(0))>0)
				{
					return 1;
				}
				else if(o1.getGenerosAsociados().darElemento(0).compareTo(o2.getGenerosAsociados().darElemento(0))<0)
				{
					return -1;
				}
				else
				{
					if(o1.getRatingIMBD() > o2.getRatingIMBD())
					{
						return 1;
					}
					else if(o1.getRatingIMBD() < o2.getRatingIMBD())
					{
						return -1;
					}
					else
					{
						return 0;
					}
				}
			}
		}
		
	}

	
}
