package api;

import java.util.Iterator;

import model.data_estructuras.*;


public interface ILista<T> {
	
public void agregarElementoFinal(T elem);
	
	public Nodo<T> darCabeza();
	
	public void agregarElmentoInicio(T elem);
	
	public T eliminarElemento();
	
	public T darElemento(int pos);
	
	public void cambiar(int pos,T dato);
	
	public int darNumeroElementos();
	
	public T darElementoPosicionActual();
	
	public boolean avanzarSiguientePosicion();
	
	public boolean retrocederPosicionAnterior();
	
	public void eliminarNodo (T elem);
	
	public Iterator<T> iterator();
}
