package api;

import java.util.Comparator;

import model.vo.VOPeliculaRating;

public class Comparador1B implements Comparator<VOPeliculaRating>{

	public int compare(VOPeliculaRating o1, VOPeliculaRating o2) {
		if(o1.getRating().doubleValue() < o2.getRating().doubleValue())
			return -1;
		else if(o1.getRating().doubleValue() == o2.getRating().doubleValue() )
			return 0;
		else
			return 1;
	}
	

}
