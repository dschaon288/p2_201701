package api;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.DateFormatSymbols;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;

import javax.swing.text.DateFormatter;

import model.data_estructuras.ColaPrioridad;
import model.data_estructuras.EncadenamientoSeparadoTH;
import model.data_estructuras.ListaEncadenada;
import model.data_estructuras.RedBlackBST;
import model.data_estructuras.ListaLlaveValorSecuencial;
import model.vo.*;
import api.ISistemaRecomendacionPeliculas.segmentosClientes;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class SistemaRecomendacion implements ISistemaRecomendacionPeliculas
{
	private EncadenamientoSeparadoTH<Long, VOPelicula> listaPeliculas;
	private EncadenamientoSeparadoTH<Integer, VOUsuario> listaUsuarios;
	private EncadenamientoSeparadoTH<Integer, VOTag> listaTags;
	
	private ColaPrioridad<VOUsuarioEntrada> colaPrioridad;
	private RedBlackBST<Integer, VOUsuario> arbolAyuda;
	
	private EncadenamientoSeparadoTH<Long,VOSimilitud> similitudes;
	private RedBlackBST<Integer, EncadenamientoSeparadoTH<String,RedBlackBST<Double, model.data_estructuras.ListaLlaveValorSecuencial<VOPelicula, EncadenamientoSeparadoTH<VOTag,VOUsuarioPelicula>>>>> arbolCompletoGigante;
	private EncadenamientoSeparadoTH<String,Integer> nombresGenero;
	private EncadenamientoSeparadoTH<VOGeneroPelicula, VOPelicula> generosPeliculas;
	
	
	private RedBlackBST<String, String> diccionario;
	private ListaEncadenada<segmentosClientes> listaSegmentos;
	private segmentosClientes noClasificados ;
	private segmentosClientes inconformes;
	private segmentosClientes conformes;
	private segmentosClientes neutrales;

	@Override
	public ISistemaRecomendacionPeliculas crearSR() {

		SistemaRecomendacion x = new SistemaRecomendacion();
		listaPeliculas = new EncadenamientoSeparadoTH<>(1000);
		listaUsuarios = new EncadenamientoSeparadoTH<>(100);
		listaTags = new EncadenamientoSeparadoTH<>(1000);
		
		
		nombresGenero = new EncadenamientoSeparadoTH<>(100);
		generosPeliculas = new EncadenamientoSeparadoTH<>(1000);
		
		arbolCompletoGigante = new RedBlackBST<>();		
		colaPrioridad = new ColaPrioridad(100);
		arbolAyuda = new RedBlackBST<>();
		diccionario = new RedBlackBST<>();
		
	
		
		
		listaSegmentos = new ListaEncadenada<>();
		noClasificados = segmentosClientes.No_Clasificados;
		inconformes = segmentosClientes.Inconformes;
		conformes = segmentosClientes.Conformes;
		neutrales = segmentosClientes.Neutrales;
		return x;
	}
	public EncadenamientoSeparadoTH<Long, VOPelicula> getPelis(){
		return listaPeliculas;
	}

	@Override
	public boolean cargarPeliculasSR(String rutaPeliculas) {
		try{
			FileReader fr = new FileReader(rutaPeliculas);
			JsonParser pr = new JsonParser();
			JsonArray obj = (JsonArray) pr.parse(fr);
			for(int i = 0; i < obj.size() ; i++){
				JsonObject tit = (JsonObject) obj.get(i);
				JsonObject data = (JsonObject) tit.get("imdbData");
				VOPelicula nueva = new VOPelicula();
				String ids =  tit.get("movieId").getAsString();
				long id = Long.parseLong(ids);
				String pa = data.get("Country").getAsString();
				nueva.setPaisito(pa);
				String nombre = data.get("Title").getAsString();
				nueva.setNombre(nombre);
				int ye = 0;
				try{
					String year = data.get("Year").getAsString();
					ye = Integer.parseInt(year);
				}catch (Exception e) {
					ye = 0;
				}
				nueva.setYear(ye);
				double rait = 0;
				try{
					String ra = data.get("imdbRating").getAsString();
					rait = Double.parseDouble(ra);	
				}catch (Exception e) {
					rait = 0;
				}
				nueva.setRatingIMBD(rait);
				DateFormat fmt = new SimpleDateFormat("dd MMM yyyy");
				Date fecha = new Date();
				try{
					String an =data.get("Released").getAsString();
					fecha = fmt.parse(an);
				}catch (Exception e) {

				}
				nueva.setFechaLanzamineto(fecha);
				String generos = data.get("Genre").getAsString();
				String x[] = generos.split(",");
				ILista<VOGeneroPelicula> gene = new ListaEncadenada<>();
				for(int j = 0; j < x.length; j++){
					x[j] = x[j].replaceAll(" ", "");
					VOGeneroPelicula genero = new VOGeneroPelicula();
					genero.setNombre(x[j]);
					gene.agregarElementoFinal(genero);
				}
				nueva.setGenerosAsociados(gene);
				String vot = data.get("imdbVotes").getAsString();
				try{
					String y[] = vot.split(",");
					vot = "";
					for (int j = 0; j < y.length; j++) {
						vot += y[j];
					}
					nueva.setVotostotales(Integer.valueOf(vot));
				}catch (Exception e) {
					// TODO: handle exception
					nueva.setVotostotales(0);
				}				
				int temp = 2017 - nueva.getYear();
				double votas = nueva.getVotostotales()/temp;
				if(nueva.getYear()==0){
					votas = 0;
				}
				nueva.setPromedioAnualVotos(votas);
				listaPeliculas.insertar(id, nueva);
				Iterator<VOGeneroPelicula> iter = gene.iterator();
				int xx = 0;
				while (iter.hasNext()) {
					VOGeneroPelicula voGene = (VOGeneroPelicula) iter.next();
					generosPeliculas.insertar(voGene, nueva);
					nombresGenero.insertar(voGene.getNombre(),xx);
					xx++;
				}
			}
		}catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	@Override
	public boolean cargarRatingsSR(String rutaRaitings) {
		try {
			BufferedReader br = new BufferedReader(new FileReader(rutaRaitings));
			String line = br.readLine();
			line = br.readLine();
			while (line !=null){
				VOUsuarioPelicula nuevo = new VOUsuarioPelicula();
				String x[] = line.split(",");
				nuevo.setIdUsuario(Integer.parseInt(x[0]));
				nuevo.setIdPeliculon(Long.parseLong(x[1]));
				nuevo.setNombrepelicula(listaPeliculas.darValor(Long.parseLong(x[1])).getNombre());
				nuevo.setRatingUsuario(Double.parseDouble(x[2]));
				Timestamp tms = new Timestamp(Long.parseLong(x[3]));
				Date fecha = new Date(tms.getDate());
				nuevo.setfPrimerR(fecha);
				if(!listaUsuarios.tieneLlave(Integer.parseInt(x[0]))){
					VOUsuario actual = new VOUsuario();
					actual.setIdUsuario(Integer.parseInt(x[0]));
					actual.setTimeS(fecha);
					ListaEncadenada<VOUsuarioPelicula> jeje = new ListaEncadenada<>();
					jeje.agregarElementoFinal(nuevo);
					actual.setNose(jeje);
					listaUsuarios.insertar(Integer.parseInt(x[0]), actual);
				}
				else{
					VOUsuario xx = listaUsuarios.darValor(Integer.parseInt(x[0]));
					if(xx.getTimeS().after(fecha)){
						xx.setTimeS(fecha);
					}
					xx.getNose().agregarElementoFinal(nuevo);
				}
				line = br.readLine();
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;

	}

	@Override
	public boolean cargarTagsSR(String rutaTags) {
		int i = 1;
		try{
			BufferedReader br = new BufferedReader(new FileReader(rutaTags));
			String line = br.readLine();
			line = br.readLine();
			while (line !=null){
				String x[] = line.split(",");
				VOTag  nuevo= new VOTag();
				try{
					nuevo.setIdUsuarios(Integer.valueOf(x[0]));
				}
				catch(Exception e)
				{
					for(int j = 0; j < x.length; j++){
						x[j] = x[j].replaceAll("\"", "");
					}
					nuevo.setIdUsuarios(Integer.valueOf(x[0]));
				}			
				nuevo.setIdPelicula(Long.valueOf(x[1]));
				nuevo.setContenido(x[2]);
				VOUsuario us = listaUsuarios.darValor(nuevo.getIdUsuarios());
				Iterator<VOUsuarioPelicula> iter = us.getNose().iterator();
				while (iter.hasNext()) {
					VOUsuarioPelicula voUs = (VOUsuarioPelicula) iter.next();
					if(voUs.getIdPeliculon().equals(Long.valueOf(x[1]))){
						if(voUs.getTags()!=null){
							voUs.getTags().agregarElementoFinal(nuevo);
						}else{
							ILista<VOTag> serDecentes = new ListaEncadenada<>();
							serDecentes.agregarElementoFinal(nuevo);
							voUs.setTags(serDecentes);
						}
					}
				}
				listaTags.insertar(i, nuevo);
				i++;
				line = br.readLine();


			}
			Iterator<Integer> iter20 = listaUsuarios.llaves();
			while(iter20.hasNext()){
				VOUsuario ustere = listaUsuarios.darValor(iter20.next());
				Iterator<VOUsuarioPelicula> itertt = ustere.getNose().iterator();
				while (itertt.hasNext()) {
					VOUsuarioPelicula voUs = (VOUsuarioPelicula) itertt.next();
					if(voUs.getTags() == null){
						ILista<VOTag> serDecentes = new ListaEncadenada<>();
						voUs.setTags(serDecentes);
					}
				}
			}
		}catch (Exception e) {

			e.printStackTrace();
			System.out.println(i);
			return false;
		}
		return true;

	}

	public void crearSimiltudes(){
		Iterator<Long> iter = listaPeliculas.llaves();
		while (iter.hasNext()) {
			Long id = (Long) iter.next();
			VOPelicula temp = listaPeliculas.darValor(id);
			Iterator<Long> iter2 = listaPeliculas.llaves();
			while (iter2.hasNext()) {
				VOSimilitud similitudConAmor = new VOSimilitud();
				Long long1 = (Long) iter2.next();
				VOPelicula temp1 = listaPeliculas.darValor(long1);
				if(!temp1.equals(temp)){
					Iterator<Integer> iter3 = listaUsuarios.llaves();
					ListaEncadenada<VORatingRatingconAmor> amor = new ListaEncadenada<>(); 
					while (iter3.hasNext()) {
						Integer in = (Integer) iter3.next();
						VOUsuario usua = listaUsuarios.darValor(in);
						Iterator<VOUsuarioPelicula> iter4 = usua.getNose().iterator();
						int cuenta = 0;
						VORatingRatingconAmor rankingsito = new VORatingRatingconAmor();
						boolean ayuda = false;
						while (iter4.hasNext() && !ayuda) {
							VOUsuarioPelicula usuaP1 = (VOUsuarioPelicula) iter4.next();
							if (cuenta == 0){
								if(usuaP1.getNombrepelicula().equals(temp.getNombre()) || usuaP1.getNombrepelicula().equals(temp1.getNombre())){
									cuenta = 1;
									rankingsito.setRanking1(usuaP1.getRatingUsuario());
								}
							}
							else{
								if(usuaP1.getNombrepelicula().equals(temp.getNombre()) || usuaP1.getNombrepelicula().equals(temp1.getNombre())){
									rankingsito.setRanking2(usuaP1.getRatingUsuario());
									ayuda = true;
								}
							}
						}
						System.out.println(ayuda);
						if(ayuda){
							amor.agregarElementoFinal(rankingsito);
						}
					}
					if(amor.darNumeroElementos() < 3){
						similitudConAmor.setIdComparador(long1);
						similitudConAmor.setSimil(0);
					}
					else{
						Iterator<VORatingRatingconAmor> iterAmor = amor.iterator();
						double numerdaor = 0;
						double denominadorA = 0 , denominadorB = 0 ;
						while (iterAmor.hasNext()) {
							VORatingRatingconAmor voRA = (VORatingRatingconAmor) iterAmor.next();
							numerdaor = numerdaor + ( voRA.getRanking1() * voRA.getRanking2());
							denominadorA += (voRA.getRanking1()*voRA.getRanking1());
							denominadorB += (voRA.getRanking2()*voRA.getRanking2());
						}
						similitudConAmor.setIdComparador(long1);
						similitudConAmor.setSimil(numerdaor/(denominadorA*denominadorB));
					}
					similitudes.insertar(id, similitudConAmor);
				}
			}
		}
	}

	public void crearErrores(int idPelicula, int idUsuario, Double rating){
		ListaEncadenada<VOSimilitud> simi = new ListaEncadenada<>();
		VOPelicula temp = listaPeliculas.darValor(Long.valueOf(idPelicula));
		VOUsuario user = listaUsuarios.darValor(idUsuario);
		arbolAyuda.put(idUsuario, user);
		Iterator<VOUsuarioPelicula> iter1 = user.getNose().iterator();
		ListaEncadenada<Long> listaids = new ListaEncadenada<>();
		while (iter1.hasNext()) {
			VOUsuarioPelicula xx = iter1.next();
			listaids.agregarElementoFinal(xx.getIdPeliculon());
		}
		Iterator<Long> iter2 = listaids.iterator();
		while (iter2.hasNext()) {
			VOSimilitud similitudConAmor = new VOSimilitud();
			Long long1 = (Long) iter2.next();
			VOPelicula temp1 = listaPeliculas.darValor(long1);
			Iterator<Integer> iter3 = listaUsuarios.llaves();
			ListaEncadenada<VORatingRatingconAmor> amor = new ListaEncadenada<>(); 
			while (iter3.hasNext()) {
				Integer in = (Integer) iter3.next();
				VOUsuario usua = listaUsuarios.darValor(in);
				Iterator<VOUsuarioPelicula> iter4 = usua.getNose().iterator();
				int cuenta = 0;
				VORatingRatingconAmor rankingsito = new VORatingRatingconAmor();
				boolean ayuda = false;
				while (iter4.hasNext() && !ayuda) {
					VOUsuarioPelicula usuaP1 = (VOUsuarioPelicula) iter4.next();
					if (cuenta == 0){
						if(usuaP1.getNombrepelicula().equals(temp.getNombre()) || usuaP1.getNombrepelicula().equals(temp1.getNombre())){
							cuenta = 1;
							rankingsito.setRanking1(usuaP1.getRatingUsuario());
						}
					}
					else{
						if(usuaP1.getNombrepelicula().equals(temp.getNombre()) || usuaP1.getNombrepelicula().equals(temp1.getNombre())){
							rankingsito.setRanking2(usuaP1.getRatingUsuario());
							ayuda = true;
						}
					}
				}
				if(ayuda){
					amor.agregarElementoFinal(rankingsito);
				}
			}
			if(amor.darNumeroElementos() < 3){
				similitudConAmor.setIdComparador(long1);
				similitudConAmor.setSimil(0);
			}
			else{
				Iterator<VORatingRatingconAmor> iterAmor = amor.iterator();
				double numerdaor = 0;
				double denominadorA = 0 , denominadorB = 0 ;
				while (iterAmor.hasNext()) {
					VORatingRatingconAmor voRA = (VORatingRatingconAmor) iterAmor.next();
					numerdaor = numerdaor + ( voRA.getRanking1() * voRA.getRanking2());
					denominadorA += (voRA.getRanking1()*voRA.getRanking1());
					denominadorB += (voRA.getRanking2()*voRA.getRanking2());
				}
				similitudConAmor.setIdComparador(long1);
				similitudConAmor.setSimil(numerdaor/(Math.sqrt(denominadorA))*(Math.sqrt(denominadorB)));
			}
			simi.agregarElementoFinal(similitudConAmor);
		}
		Iterator<VOSimilitud> iter20 = simi.iterator();
		iter1 = user.getNose().iterator();
		double prediccion= 0;
		double numerador = 0;
		double denominador = 0; 
		while (iter20.hasNext()) {
			VOSimilitud voS = (VOSimilitud) iter20.next();
			VOUsuarioPelicula voU = iter1.next();
			numerador += voS.getSimil()* voU.getRatingUsuario();
			denominador += voS.getSimil();
		}
		prediccion = numerador/denominador;
		VOUsuarioPelicula voU = new VOUsuarioPelicula();
		voU.setIdUsuario(idUsuario);
		voU.setIdPeliculon(Long.valueOf(idPelicula));
		voU.setNombrepelicula(listaPeliculas.darValor(Long.valueOf(idPelicula)).getNombre());
		voU.setRatingUsuario(rating);
		Iterator<VOUsuarioPelicula> iter20mil = user.getNose().iterator();
		while (iter20mil.hasNext()) {
			VOUsuarioPelicula voUs = (VOUsuarioPelicula) iter20mil.next();
			if(voUs.getIdPeliculon().equals(voU.getIdPeliculon())){
				voUs.setRatingSistema(prediccion);
				voUs.setErrorRating(Math.abs(voU.getRatingSistema()-rating));
			}
		}

	}

	@Override
	public int sizeMoviesSR() {
		return listaPeliculas.darTamanio();
	}

	@Override
	public int sizeUsersSR() {
		return listaUsuarios.darTamanio();
	}

	@Override
	public int sizeTagsSR() {
		return listaTags.darTamanio();
	}

	@Override
	public void registrarSolicitudRecomendacion(Integer idUsuario, String ruta) {
		
		Comparador1A c = new Comparador1A();
		Comparador1B c2 = new Comparador1B();
		if(idUsuario == null && ruta !=null){
			try{
				FileReader fr = new FileReader(ruta);
				JsonParser pr = new JsonParser();
				JsonObject tit = (JsonObject) pr.parse(fr);
				JsonObject data = (JsonObject) tit.get("request");
				JsonArray rat = (JsonArray) data.get("ratings");
				String name = data.get("user_name").getAsString();
				VOUsuarioEntrada nuevo = new VOUsuarioEntrada();
				nuevo.setUsuarioName(name);
				ILista<VOPeliculaRating> list = new ListaEncadenada<>();
				for (int i = 0; i < rat.size(); i++) {
					JsonObject pel = (JsonObject) rat.get(i);
					Long id = pel.get("item_id").getAsLong();
					Number rait =  pel.get("rating").getAsNumber();
					VOPeliculaRating peli = new VOPeliculaRating();
					peli.setId(id);
					peli.setRating(rait);
					list.agregarElementoFinal(peli);
				}
				colaPrioridad.agregar(nuevo,c);
			}
			catch (Exception e) {
				e.printStackTrace();
			}
		}
		else if( idUsuario !=null && ruta ==null){
			try {
				VOUsuario usu = listaUsuarios.darValor(idUsuario);
				VOUsuarioEntrada nuevo = new VOUsuarioEntrada();
				nuevo.setUsu(usu);
				Iterator<VOUsuarioPelicula> iter = usu.getNose().iterator();
				while (iter.hasNext()) {
					VOUsuarioPelicula voUs = (VOUsuarioPelicula) iter.next();
					crearErrores(voUs.getIdPeliculon().intValue(), idUsuario, voUs.getRatingUsuario());
				}
				iter = usu.getNose().iterator();
				ColaPrioridad<VOPeliculaRating> cp = new ColaPrioridad<>(100);
				while (iter.hasNext()) {
					VOUsuarioPelicula voUs = (VOUsuarioPelicula) iter.next();
					VOPeliculaRating peliRat = new VOPeliculaRating();
					peliRat.setId(voUs.getIdPeliculon());
					peliRat.setRating(voUs.getRatingSistema());
					cp.agregar(peliRat,c2);
				}
				nuevo.setPeliculitasRecomendadas(cp);
				usu.setEntrada(nuevo);
				colaPrioridad.agregar(nuevo,c);
			}
			catch (Exception e) {
				
				e.printStackTrace();
			}
		}
		else{
			System.out.println("Error no entro nada o entro ambos");
		}
	}

	@Override
	public void generarRespuestasRecomendaciones() {
		
		if(!colaPrioridad.isEmpty()){
			JsonObject obj = new JsonObject();
			JsonObject obj1 = new JsonObject();
			JsonArray arra = new JsonArray();
			obj.add("response", obj1);
			for (int i = 0; i < 10 && !colaPrioridad.isEmpty(); i++) {
				JsonObject obj2 = new JsonObject();
				VOUsuarioEntrada tem = colaPrioridad.max();
				if(tem.esUsuario()){
					JsonArray arra1 =  new JsonArray();
					for (int j = 0; j < 5 ; j++) {
						JsonObject obj3 = new JsonObject();
						VOPeliculaRating temp = tem.getPeliculitasRecomendadas().max();
						obj3.addProperty("item_id",temp.getId());
						obj3.addProperty("p_rating", temp.getRating());
						arra1.add(obj3);
					}
					obj2.add("recomedations", arra1);
					obj2.addProperty("user_id", tem.getUsu().getIdUsuario());
				}
				else
				{
					JsonArray arra1 =  new JsonArray();
					for (int j = 0; j < 5 ; j++) {
						JsonObject obj3 = new JsonObject();
						VOPeliculaRating temp = tem.getPeliculitasRecomendadas().max();
						obj3.addProperty("item_id", temp.getId());
						obj3.addProperty("p_rating", temp.getRating());
						arra1.add(obj3);
					}
					obj2.add("recomedations", arra1);
					obj2.addProperty("user_id", tem.getUsuarioName());
				}
				arra.add(obj2);
			}
			obj1.add("response_users", arra);

			try (FileWriter file = new FileWriter("./data/response1.json")) {
				file.write(obj.toString());
			} catch (Exception e) {

				e.printStackTrace();
			}
		}
	}

	@Override
	public ILista<VOPelicula> peliculasGenero(VOGeneroPelicula genero, Date fechaInicial, Date fechaFinal) {
		
		
		ListaEncadenada<VOPelicula> listaGeneros = new ListaEncadenada<>();
		
		RedBlackBST<Date, VOPelicula> arboli = new RedBlackBST<>();
		EncadenamientoSeparadoTH<VOGeneroPelicula, RedBlackBST<Date, VOPelicula>> generos = new EncadenamientoSeparadoTH<>(1000);
		Iterator<Long> iter = listaPeliculas.llaves();
		
		while (iter.hasNext()) {
			Long id = (Long) iter.next();
			VOPelicula peliActual = listaPeliculas.darValor(id);
			arboli.put(peliActual.getFechaLanzamineto(), peliActual);
		}
		Iterator<VOGeneroPelicula> iter2 = generosPeliculas.llaves();
		
		while (iter2.hasNext()) {
			VOGeneroPelicula voGene = (VOGeneroPelicula) iter2.next();
			if(arboli.contains(generosPeliculas.darValor(voGene).getFechaLanzamineto()))
				generos.insertar(voGene, arboli);
		}
		iter2 = generos.llaves();
		
		while (iter2.hasNext()) {
			VOGeneroPelicula voGene = (VOGeneroPelicula) iter2.next();
			if(voGene.getNombre().equals(genero.getNombre())){
				Date fechaLanzamiento = generosPeliculas.darValor(voGene).getFechaLanzamineto();
				if(fechaLanzamiento.before(fechaFinal)  && fechaLanzamiento.after(fechaInicial)){
					listaGeneros.agregarElementoFinal(generos.darValor(voGene).get(generosPeliculas.darValor(voGene).getFechaLanzamineto()));
				}
			}
		}
		return listaGeneros;
	}

	@Override
	public void agregarRatingConError(int idUsuario, int idPelicula, Double rating) {

		ListaEncadenada<VOSimilitud> simi = new ListaEncadenada<>();
		VOPelicula temp = listaPeliculas.darValor(Long.valueOf(idPelicula));
		VOUsuario user = listaUsuarios.darValor(idUsuario);
		Iterator<VOUsuarioPelicula> iter1 = user.getNose().iterator();
		ListaEncadenada<Long> listaids = new ListaEncadenada<>();
		while (iter1.hasNext()) {
			VOUsuarioPelicula xx = iter1.next();
			listaids.agregarElementoFinal(xx.getIdPeliculon());
		}
		Iterator<Long> iter2 = listaids.iterator();
		while (iter2.hasNext()) {
			VOSimilitud similitudConAmor = new VOSimilitud();
			Long long1 = (Long) iter2.next();
			VOPelicula temp1 = listaPeliculas.darValor(long1);
			Iterator<Integer> iter3 = listaUsuarios.llaves();
			ListaEncadenada<VORatingRatingconAmor> amor = new ListaEncadenada<>(); 
			while (iter3.hasNext()) {
				Integer in = (Integer) iter3.next();
				VOUsuario usua = listaUsuarios.darValor(in);
				Iterator<VOUsuarioPelicula> iter4 = usua.getNose().iterator();
				int cuenta = 0;
				VORatingRatingconAmor rankingsito = new VORatingRatingconAmor();
				boolean ayuda = false;
				while (iter4.hasNext() && !ayuda) {
					VOUsuarioPelicula usuaP1 = (VOUsuarioPelicula) iter4.next();
					if (cuenta == 0){
						if(usuaP1.getNombrepelicula().equals(temp.getNombre()) || usuaP1.getNombrepelicula().equals(temp1.getNombre())){
							cuenta = 1;
							rankingsito.setRanking1(usuaP1.getRatingUsuario());
						}
					}
					else{
						if(usuaP1.getNombrepelicula().equals(temp.getNombre()) || usuaP1.getNombrepelicula().equals(temp1.getNombre())){
							rankingsito.setRanking2(usuaP1.getRatingUsuario());
							ayuda = true;
						}
					}
				}
				if(ayuda){
					amor.agregarElementoFinal(rankingsito);
				}
			}
			if(amor.darNumeroElementos() < 3){
				similitudConAmor.setIdComparador(long1);
				similitudConAmor.setSimil(0);
			}
			else{
				Iterator<VORatingRatingconAmor> iterAmor = amor.iterator();
				double numerdaor = 0;
				double denominadorA = 0 , denominadorB = 0 ;
				while (iterAmor.hasNext()) {
					VORatingRatingconAmor voRA = (VORatingRatingconAmor) iterAmor.next();
					numerdaor = numerdaor + ( voRA.getRanking1() * voRA.getRanking2());
					denominadorA += (voRA.getRanking1()*voRA.getRanking1());
					denominadorB += (voRA.getRanking2()*voRA.getRanking2());
				}
				similitudConAmor.setIdComparador(long1);
				similitudConAmor.setSimil(numerdaor/(Math.sqrt(denominadorA)*Math.sqrt(denominadorB)));
			}
			simi.agregarElementoFinal(similitudConAmor);
		}
		Iterator<VOSimilitud> iter20 = simi.iterator();
		iter1 = user.getNose().iterator();
		double prediccion= 0;
		double numerador = 0;
		double denominador = 0; 
		while (iter20.hasNext()) {
			VOSimilitud voS = (VOSimilitud) iter20.next();
			VOUsuarioPelicula voU = iter1.next();
			numerador += voS.getSimil()* voU.getRatingUsuario();
			denominador += voS.getSimil();
		}
		prediccion = numerador/denominador;
		iter1 = user.getNose().iterator();
		VOUsuarioPelicula voU = new VOUsuarioPelicula();
		voU.setIdUsuario(idUsuario);
		voU.setIdPeliculon(Long.valueOf(idPelicula));
		voU.setNombrepelicula(listaPeliculas.darValor(Long.valueOf(idPelicula)).getNombre());
		voU.setRatingUsuario(rating);
		voU.setRatingSistema(prediccion);
		voU.setErrorRating(Math.abs(voU.getRatingSistema()-rating));
		user.getNose().agregarElementoFinal(voU);
		
		System.out.println(prediccion);
		System.out.println(voU.getRatingSistema());
		System.out.println(voU.getErrorRating());
	}

	@Override
	public ILista<VOUsuarioPelicula> informacionInteraccionUsuario(int idUsuario) {
		
		ILista<VOUsuarioPelicula> respuesta = new ListaEncadenada<>();
		VOUsuario actual = listaUsuarios.darValor(idUsuario);
		Iterator<VOUsuarioPelicula> iter = actual.getNose().iterator();
		while (iter.hasNext()) {
			VOUsuarioPelicula voUs = (VOUsuarioPelicula) iter.next();
			crearErrores(voUs.getIdPeliculon().intValue(), idUsuario, voUs.getRatingUsuario());
			System.out.println(voUs.getNombrepelicula() + "\n"+voUs.getErrorRating() + "..." + voUs.getTags().darNumeroElementos());
			respuesta.agregarElementoFinal(voUs);
		}
		
		System.out.println(actual.getEntrada().getPeliculitasRecomendadas().darNumeroElementos());
		
		return respuesta;
	}

	@Override
	public void clasificarUsuariosPorSegmento() {
		try{
			BufferedReader br = new BufferedReader(new FileReader("./data/categoriestags-v2.csv"));
			String line = br.readLine();
			line = br.readLine();
			while (line !=null){
				String x[] = line.split(",");
				diccionario.put(x[0], x[1]);
				line = br.readLine();
			}

			Iterator<Integer> iter = listaUsuarios.llaves();
			while(iter.hasNext()){
				Integer idUs = iter.next();
				VOUsuario actual = listaUsuarios.darValor(idUs);

				Iterator<VOUsuarioPelicula> iter1 = actual.getNose().iterator();
				int con = 0, inc = 0, noc =0, neut = 0;
				while (iter1.hasNext()) {
					VOUsuarioPelicula voUs = (VOUsuarioPelicula) iter1.next();

					Iterator<VOTag> iter2 = voUs.getTags().iterator();
					while (iter2.hasNext()) {
						VOTag voTag = (VOTag) iter2.next();
						String contenidoTag = voTag.getContenido();
						String clasifTag = diccionario.get(contenidoTag);
						if(clasifTag.equals("conforme"))
							con++;
						else if(clasifTag.equals("neutral"))
							neut++;
						else if(clasifTag.equals("inconforme"))
							inc++;
						else
							noc++;
					}
					if(voUs.getTags().darNumeroElementos() == 0){
						noc ++;
					}
				}
				if(con >= inc && con >= noc && con >= neut)
					conformes.getLista().agregarElementoFinal(idUs);
				else if(inc >= con && inc >= noc && inc >= neut)
					inconformes.getLista().agregarElementoFinal(idUs);
				else if(noc >= con && noc >= inc && noc >= neut)
					noClasificados.getLista().agregarElementoFinal(idUs);
				else
					neutrales.getLista().agregarElementoFinal(idUs);

			}

			listaSegmentos.agregarElementoFinal(conformes);
			listaSegmentos.agregarElementoFinal(inconformes);
			listaSegmentos.agregarElementoFinal(neutrales);
			listaSegmentos.agregarElementoFinal(noClasificados);

		}catch(Exception e){

		}



	}

	@Override
	public void ordenarPeliculasPorAnho() {
		Iterator<Long> iter = listaPeliculas.llaves();

		while (iter.hasNext()) {
			Long idPelicula = (Long) iter.next();
			VOPelicula peliActual = listaPeliculas.darValor(idPelicula);

			if(arbolCompletoGigante.contains(peliActual.getYear()))
			{
				Iterator<VOGeneroPelicula> iter1 = peliActual.getGenerosAsociados().iterator();

				while (iter1.hasNext()) {
					VOGeneroPelicula voGeneroPelicula = (VOGeneroPelicula) iter1.next();
					boolean t = arbolCompletoGigante.get(peliActual.getYear()).tieneLlave(voGeneroPelicula.getNombre());
					if(t)
					{
						if(arbolCompletoGigante.get(peliActual.getYear()).darValor(voGeneroPelicula.getNombre()).contains(peliActual.getRatingIMBD())){

							if(arbolCompletoGigante.get(peliActual.getYear()).darValor(voGeneroPelicula.getNombre()).get(peliActual.getRatingIMBD()).tieneLlave(peliActual)){
								Iterator<Integer> iter2 = listaUsuarios.llaves();
								while (iter2.hasNext()) {
									Integer idUsuarios = (Integer) iter2.next();
									Iterator<VOUsuarioPelicula> iter3 = listaUsuarios.darValor(idUsuarios).getNose().iterator();
									while (iter3.hasNext()) {
										VOUsuarioPelicula voUsuarioPelicula = (VOUsuarioPelicula) iter3.next();
										Iterator<VOTag> iter4 = voUsuarioPelicula.getTags().iterator();
										while (iter4.hasNext()) {
											VOTag voTag = (VOTag) iter4.next();
											if(listaPeliculas.darValor(voTag.getIdPelicula()).equals(peliActual)){
												if(arbolCompletoGigante.get(peliActual.getYear()).darValor(voGeneroPelicula.getNombre()).get(peliActual.getRatingIMBD()).darValor(peliActual).tieneLlave(voTag)){

												}
												else{
													arbolCompletoGigante.get(peliActual.getYear()).darValor(voGeneroPelicula.getNombre()).get(peliActual.getRatingIMBD()).darValor(peliActual).insertar(voTag, voUsuarioPelicula);
												}
											}
										}
									}
								}
							}
							else{
								EncadenamientoSeparadoTH<VOTag,VOUsuarioPelicula> tagsitosUsuarios = new EncadenamientoSeparadoTH<>(5);
								Iterator<Integer> iter2 = listaUsuarios.llaves();
								while (iter2.hasNext()) {
									Integer idUsuarios = (Integer) iter2.next();
									Iterator<VOUsuarioPelicula> iter3 = listaUsuarios.darValor(idUsuarios).getNose().iterator();
									while (iter3.hasNext()) {
										VOUsuarioPelicula voUsuarioPelicula = (VOUsuarioPelicula) iter3.next();
										Iterator<VOTag> iter4 = voUsuarioPelicula.getTags().iterator();
										while (iter4.hasNext()) {
											VOTag voTag = (VOTag) iter4.next();
											if(listaPeliculas.darValor(voTag.getIdPelicula()).equals(peliActual)){
												tagsitosUsuarios.insertar(voTag, voUsuarioPelicula);
											}
										}
									}
								}
								arbolCompletoGigante.get(peliActual.getYear()).darValor(voGeneroPelicula.getNombre()).get(peliActual.getRatingIMBD()).insertar(peliActual, tagsitosUsuarios);
							}
						}
						else{
							ListaLlaveValorSecuencial<VOPelicula, EncadenamientoSeparadoTH<VOTag,VOUsuarioPelicula>> listPeliTag = new ListaLlaveValorSecuencial<>();
							EncadenamientoSeparadoTH<VOTag,VOUsuarioPelicula> tagsitosUsuarios = new EncadenamientoSeparadoTH<>(5);
							Iterator<Integer> iter2 = listaUsuarios.llaves();
							while (iter2.hasNext()) {
								Integer idUsuarios = (Integer) iter2.next();
								Iterator<VOUsuarioPelicula> iter3 = listaUsuarios.darValor(idUsuarios).getNose().iterator();
								while (iter3.hasNext()) {
									VOUsuarioPelicula voUsuarioPelicula = (VOUsuarioPelicula) iter3.next();
									Iterator<VOTag> iter4 = voUsuarioPelicula.getTags().iterator();
									while (iter4.hasNext()) {
										VOTag voTag = (VOTag) iter4.next();
										if(listaPeliculas.darValor(voTag.getIdPelicula()).equals(peliActual)){
											tagsitosUsuarios.insertar(voTag, voUsuarioPelicula);
										}
									}
								}
							}
							listPeliTag.insertar(peliActual, tagsitosUsuarios);
							arbolCompletoGigante.get(peliActual.getYear()).darValor(voGeneroPelicula.getNombre()).put(peliActual.getRatingIMBD(), listPeliTag);
						}
					}
					else{
						RedBlackBST<Double, ListaLlaveValorSecuencial<VOPelicula, EncadenamientoSeparadoTH<VOTag,VOUsuarioPelicula>>> arbolitoMedio = new RedBlackBST<>();
						ListaLlaveValorSecuencial<VOPelicula, EncadenamientoSeparadoTH<VOTag,VOUsuarioPelicula>> listPeliTag = new ListaLlaveValorSecuencial<>();
						EncadenamientoSeparadoTH<VOTag,VOUsuarioPelicula> tagsitosUsuarios = new EncadenamientoSeparadoTH<>(5);
						Iterator<Integer> iter2 = listaUsuarios.llaves();
						while (iter2.hasNext()) {
							Integer idUsuarios = (Integer) iter2.next();
							Iterator<VOUsuarioPelicula> iter3 = listaUsuarios.darValor(idUsuarios).getNose().iterator();
							while (iter3.hasNext()) {
								VOUsuarioPelicula voUsuarioPelicula = (VOUsuarioPelicula) iter3.next();
								Iterator<VOTag> iter4 = voUsuarioPelicula.getTags().iterator();
								while (iter4.hasNext()) {
									VOTag voTag = (VOTag) iter4.next();
									if(listaPeliculas.darValor(voTag.getIdPelicula()).equals(peliActual)){
										tagsitosUsuarios.insertar(voTag, voUsuarioPelicula);
									}
								}
							}
						}
						listPeliTag.insertar(peliActual, tagsitosUsuarios);
						arbolitoMedio.put(peliActual.getRatingIMBD(), listPeliTag);
						arbolCompletoGigante.get(peliActual.getYear()).insertar(voGeneroPelicula.getNombre(), arbolitoMedio);
					}
				}
			}
			else{
				EncadenamientoSeparadoTH<String,RedBlackBST<Double, ListaLlaveValorSecuencial<VOPelicula, EncadenamientoSeparadoTH<VOTag,VOUsuarioPelicula>>>> casitoTodito = new EncadenamientoSeparadoTH<>(20);
				Iterator<VOGeneroPelicula> iter1 = peliActual.getGenerosAsociados().iterator();
				while (iter1.hasNext()) {
					VOGeneroPelicula generoActual = (VOGeneroPelicula) iter1.next();
					RedBlackBST<Double, ListaLlaveValorSecuencial<VOPelicula, EncadenamientoSeparadoTH<VOTag,VOUsuarioPelicula>>> arbolitoMedio = new RedBlackBST<>();
					ListaLlaveValorSecuencial<VOPelicula, EncadenamientoSeparadoTH<VOTag,VOUsuarioPelicula>> listPeliTag = new ListaLlaveValorSecuencial<>();
					EncadenamientoSeparadoTH<VOTag,VOUsuarioPelicula> tagsitosUsuarios = new EncadenamientoSeparadoTH<>(5);
					Iterator<Integer> iter2 = listaUsuarios.llaves();
					while (iter2.hasNext()) {
						Integer idUsuarios = (Integer) iter2.next();
						Iterator<VOUsuarioPelicula> iter3 = listaUsuarios.darValor(idUsuarios).getNose().iterator();
						while (iter3.hasNext()) {
							VOUsuarioPelicula voUsuarioPelicula = (VOUsuarioPelicula) iter3.next();
							Iterator<VOTag> iter4 = voUsuarioPelicula.getTags().iterator();
							while (iter4.hasNext()) {
								VOTag voTag = (VOTag) iter4.next();
								if(listaPeliculas.darValor(voTag.getIdPelicula()).equals(peliActual)){
									tagsitosUsuarios.insertar(voTag, voUsuarioPelicula);
								}
							}
						}
					}
					listPeliTag.insertar(peliActual, tagsitosUsuarios);
					arbolitoMedio.put(peliActual.getRatingIMBD(), listPeliTag);
					casitoTodito.insertar(generoActual.getNombre(), arbolitoMedio);
				}
				arbolCompletoGigante.put(peliActual.getYear(), casitoTodito);
			}
		}
	}

	@Override
	public VOReporteSegmento generarReporteSegmento(String segmento) {
		
		VOReporteSegmento respuesta = new VOReporteSegmento();
		
		Iterator<segmentosClientes> iter = listaSegmentos.iterator();
		
		segmentosClientes actual = null; 
		
		while(iter.hasNext() && actual==null){
			segmentosClientes x = iter.next();			
			if (x.name().equalsIgnoreCase(segmento)){
				actual = x;
			}
		}

		EncadenamientoSeparadoTH<String, ListaEncadenada<VOPelicula>> auxilio = new EncadenamientoSeparadoTH<>(100);
		
		double sumaErrores = 0;
		int cuenta = 0;
		
		Iterator<Integer> iterIdUsers = actual.getLista().iterator();
		while(iterIdUsers.hasNext()){
			int llave = iterIdUsers.next();
			VOUsuario vo = listaUsuarios.darValor(llave);
			Iterator<VOUsuarioPelicula> iter2 = vo.getNose().iterator();
			while(iter2.hasNext()){
				VOUsuarioPelicula vousp = iter2.next();
				if(vousp.getErrorRating() != 0){
					sumaErrores += vousp.getErrorRating();
					cuenta++;
				}
				VOPelicula vopeli = listaPeliculas.darValor(vousp.getIdPeliculon());
				Iterator<VOGeneroPelicula> iterGen = vopeli.getGenerosAsociados().iterator();
				ListaEncadenada<VOPelicula> listaTemporal = new ListaEncadenada<>();
				while(iterGen.hasNext()){
					listaTemporal.agregarElementoFinal(vopeli);
					auxilio.insertar(iterGen.next().getNombre(), listaTemporal);
				}
			}

		}

		respuesta.setErrorPromedio(sumaErrores/cuenta);

		return respuesta;
	}

	@Override
	public ILista<VOPelicula> peliculasGeneroPorFechaLanzamiento(
			VOGeneroPelicula genero, Date fechaInicial, Date fechaFinal) {


		ListaEncadenada<VOPelicula> respuesta = new ListaEncadenada<>();
		RedBlackBST<Date, VOPelicula> arbolLocal = new RedBlackBST<>();
		EncadenamientoSeparadoTH<VOGeneroPelicula, RedBlackBST<Date, VOPelicula>> generos = new EncadenamientoSeparadoTH<>(1000);
		Iterator<Long> iter = listaPeliculas.llaves();
		while (iter.hasNext()) {
			Long id = (Long) iter.next();
			VOPelicula peliActual = listaPeliculas.darValor(id);
			arbolLocal.put(peliActual.getFechaLanzamineto(), peliActual);
		}

		Iterator<VOGeneroPelicula> iter2 = generosPeliculas.llaves();
		while (iter2.hasNext()) {
			VOGeneroPelicula voGene = (VOGeneroPelicula) iter2.next();
			if(arbolLocal.contains(generosPeliculas.darValor(voGene).getFechaLanzamineto()))
				generos.insertar(voGene, arbolLocal);
		}

		iter2 = generos.llaves();
		while (iter2.hasNext()) {
			VOGeneroPelicula voGene = (VOGeneroPelicula) iter2.next();
			if(voGene.getNombre().equals(genero.getNombre())){
				Date fechaLanzamiento = generosPeliculas.darValor(voGene).getFechaLanzamineto();
				if(fechaLanzamiento.before(fechaFinal)  && fechaLanzamiento.after(fechaInicial)){
					respuesta.agregarElementoFinal(generos.darValor(voGene).get(generosPeliculas.darValor(voGene).getFechaLanzamineto()));
				}
			}
		}
		return respuesta;
	}

	@Override
	public ILista<VOPelicula> peliculasMayorPrioridad(int n) {

		ColaPrioridad<VOPelicula> ordenadas = new ColaPrioridad<VOPelicula>(9500);

		Comparador10 c = new Comparador10();

		Iterator<Long> iter = listaPeliculas.llaves();
		while (iter.hasNext()) {
			Long id = (Long) iter.next();
			VOPelicula peliActual = listaPeliculas.darValor(id);
			try {
				ordenadas.agregar(peliActual, c);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		ListaEncadenada<VOPelicula> rta = new ListaEncadenada<VOPelicula>();

		for(int i = 0; i < n; i++)
		{
			rta.agregarElementoFinal(ordenadas.max());
		}
		return rta;
	}

	@Override
	public ILista<VOPelicula> consultarPeliculasFiltros(Integer anho,
			String pais, VOGeneroPelicula genero) {

		ColaPrioridad<VOPelicula> ordenadas = new ColaPrioridad<VOPelicula>(9500);
		ListaEncadenada<VOPelicula> listaFiltro = new ListaEncadenada<VOPelicula>();
		ListaEncadenada<VOPelicula> rta = new ListaEncadenada<VOPelicula>();
		
		Comparador11 c = new Comparador11();

		if(anho != 0 || !pais.equalsIgnoreCase("No") || !genero.getNombre().equalsIgnoreCase("No"))
		{
			Iterator<Long> iter = listaPeliculas.llaves();
			while (iter.hasNext()) {
				Long id = (Long) iter.next();
				VOPelicula peliActual = listaPeliculas.darValor(id);
				int y = peliActual.getYear();
				String p = peliActual.getPaisito();
				ListaEncadenada<VOGeneroPelicula> g = (ListaEncadenada<VOGeneroPelicula>) peliActual.getGenerosAsociados();
				if(anho != 0 && !pais.equalsIgnoreCase("No") && !genero.getNombre().equalsIgnoreCase("No") )
				{
					if(y == anho && pais.equalsIgnoreCase(pais))
					{
						boolean d = false;
						for(int i = 0; i < g.darNumeroElementos(); i++)
						{
							if(g.darElemento(i).getNombre().equalsIgnoreCase(genero.getNombre()))
							{
								d = true;
							}
						}

						if(d)
						{
							listaFiltro.agregarElementoFinal(peliActual);
						}
					}
				}
				else if(anho == 0)
				{
					if(!pais.equalsIgnoreCase("No") && !genero.getNombre().equalsIgnoreCase("No"))
					{
						if(pais.equalsIgnoreCase(pais))
						{
							boolean d = false;
							for(int i = 0; i < g.darNumeroElementos(); i++)
							{
								if(g.darElemento(i).getNombre().equalsIgnoreCase(genero.getNombre()))
								{
									d = true;
								}
							}

							if(d)
							{
								listaFiltro.agregarElementoFinal(peliActual);
							}
						}
					}
					else if(pais.equalsIgnoreCase("No"))
					{
						boolean d = false;
						for(int i = 0; i < g.darNumeroElementos(); i++)
						{
							if(g.darElemento(i).getNombre().equalsIgnoreCase(genero.getNombre()))
							{
								d = true;
							}
						}

						if(d)
						{
							listaFiltro.agregarElementoFinal(peliActual);
						}
					}
					else if(genero.getNombre().equalsIgnoreCase("No"))
					{
						if(pais.equalsIgnoreCase(pais))
						{
							listaFiltro.agregarElementoFinal(peliActual);
						}
					}
				}
				else if(pais.equalsIgnoreCase("No"))
				{
					if(anho != 0 && !genero.getNombre().equalsIgnoreCase("No"))
					{
						if(y == anho)
						{
							boolean d = false;
							for(int i = 0; i < g.darNumeroElementos(); i++)
							{
								if(g.darElemento(i).getNombre().equalsIgnoreCase(genero.getNombre()))
								{
									d = true;
								}
							}

							if(d)
							{
								listaFiltro.agregarElementoFinal(peliActual);
							}
						}
					}
					else if(genero.getNombre().equalsIgnoreCase("No"))
					{
						if(y == anho)
						{
							listaFiltro.agregarElementoFinal(peliActual);
						}
					}
				}
				else if(genero.getNombre().equalsIgnoreCase("No"))
				{
					if(anho != 0 && !pais.equalsIgnoreCase("No"))
					{
						if(y == anho && p.equalsIgnoreCase(pais))
						{
							listaFiltro.agregarElementoFinal(peliActual);
						}
					}
				}
			}
		}
		
		for(int p = 0; p < listaFiltro.darNumeroElementos(); p++)
		{
			try {
				ordenadas.agregar(listaFiltro.darElemento(p), c);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		for(int f = 0; f < ordenadas.darNumeroElementos(); f++)
		{
			rta.agregarElementoFinal(ordenadas.max());
		}
		return rta;
	}





}
