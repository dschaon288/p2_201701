package view;

import java.util.Scanner;

import test.GeneradorDatos;
import api.SistemaRecomendacion;
import model.data_estructuras.ColaPrioridad;
import model.data_estructuras.EncadenamientoSeparadoTH;
import model.data_estructuras.ListaEncadenada;
import model.data_estructuras.RedBlackBST;
import model.vo.VOGeneroPelicula;
import model.vo.VOPelicula;

public class Vista{
	
	public static void main(String[] args) {

		Scanner sc=new Scanner(System.in);
		boolean fin=false;
		GeneradorDatos GD= new GeneradorDatos();
		 SistemaRecomendacion MP = new SistemaRecomendacion();


		while(!fin){
			printMenu();

			int option = sc.nextInt();
			int x;
			int k;
			switch(option){
			case 1:
				System.out.println("Digite la cantidad de numeros que quiere generar ");
				x = sc.nextInt();
				int[]agnos= GD.generarNumeros(x);
				for (int i = 0; i < agnos.length; i++) {
					System.out.println(agnos[i]);
				}
				
				break;

			case 2: 
				
				System.out.println("Digite la cantidad de cadenas que quiere generar ");
				x = sc.nextInt();
				System.out.println("Digite la cantidad de veces que desea el caracter");
				k = sc.nextInt();
				String[]cadenas= GD.generarCadenas(x, k);
				for (int i = 0; i < cadenas.length; i++) {
					System.out.println(cadenas[i]);
				}
				break;
			case 3:
				MP.crearSR();
				MP.cargarPeliculasSR("data/links_json.json");
				MP.cargarRatingsSR("data/newRatings.csv");
				//MP.sim();
				//RedBlackBST<Integer, VOPeliculaSim> simi = MP.darsimili();
				//System.out.println(simi.size());
				System.out.println("A�o");
				int a = sc.nextInt();
				System.out.println("Pais");
				String p = sc.next();
				System.out.println("genero");
				VOGeneroPelicula g = new VOGeneroPelicula();
				g.setNombre(sc.next());
				ListaEncadenada<VOPelicula> rta = (ListaEncadenada<VOPelicula>) MP.consultarPeliculasFiltros(a, p, g);
				int c = 1;
				for(int i = 0; i < rta.darNumeroElementos(); i++)
				{
					VOPelicula act = rta.darElemento(i);
					ListaEncadenada<VOGeneroPelicula> gen = (ListaEncadenada<VOGeneroPelicula>) act.getGenerosAsociados();
					System.out.println(c + ". " + act.getNombre() + ", " + act.getYear() + ", " + act.getPaisito() + "; " + act.getRatingIMBD() + ":");
					for(int j = 0; j < gen.darNumeroElementos(); j++)
					{
						VOGeneroPelicula gAct = gen.darElemento(j);
						System.out.println(gAct.getNombre());
					}
					c++;
				}
				break;
				
			case 4:
				MP.crearSR();
				MP.cargarPeliculasSR("data/links_json.json");
				MP.cargarRatingsSR("data/newRatings.csv");
				System.out.println("Cuantas Peliculas");
				int h = sc.nextInt();
				ListaEncadenada<VOPelicula> rta4 = (ListaEncadenada<VOPelicula>) MP.peliculasMayorPrioridad(h);
				int counter = 1;
				for(int i = 0; i < h; i++)
				{
					VOPelicula act = rta4.darElemento(i);
					String t = act.getNombre();
					int y = act.getYear();
					int vT = act.getVotostotales();
					double pV = act.getPromedioAnualVotos();
					System.out.println(counter + ". " + t + "; " + y + "; " + vT + "; " + pV + "," + act.getPaisito());
					counter++;
				}
				break;
			case 5:
				System.out.println("Adios");
				sc.close();
				return;
			}
		}
	}

	private static void printMenu() {
		System.out.println("---------ISIS 1206 - Estructuras de datos----------");
		System.out.println("---------------------Taller 5----------------------");
		System.out.println("1.Dar a�os aleatorios entre 1950 y 2017");
		System.out.println("2.Generar cadena aleatoria");
		System.out.println("3.Cargar Achivo");
		System.out.println("4.Peliculas prioridad");
		System.out.println("5.Salir");
		System.out.println("Type the option number for the task, then press enter: (e.g., 1):");

	}


}
