package model.data_estructuras;

import java.util.Iterator;
import java.util.NoSuchElementException;

import api.ILista;

public class ListaEncadenada<T> implements ILista<T> {

	private Nodo<T> primero;
	private Nodo<T> ultimo;
	private Nodo<T> referencia;
	private int cantidad;
	private int posicionReferencia;

	public ListaEncadenada() {
		primero = null;
		ultimo = null;
		referencia = null;
		cantidad = 0;
		posicionReferencia = 0;
	}


	public Iterator<T> iterator() {
		return new Iterator<T>() {
			int posicionRe = -1;
			Nodo<T> actual = null;

			@Override
			public boolean hasNext() {
				return (posicionRe+1 < cantidad);
			}

			@Override
			public T next() {
				if(posicionRe < 0){
					actual = primero;
				}else{
					actual = actual.getSiguiente();
				}
				posicionRe++;
				return (T) actual.getDato();		

			}

			@Override
			public void remove() {

			}};
	}

	public void agregarElementoFinal(T elem) {
		// TODO Auto-generated method stub
		if(primero == null){
			ultimo = new Nodo<T>(elem, null);
			primero = ultimo;
			referencia = primero;
			cantidad++;
		}
		else{
			Nodo<T> nuevo = new Nodo<T>(elem, null);
			ultimo.setSiguiente(nuevo);
			ultimo = nuevo;			
			cantidad++;
		}

	}	
	
	public Nodo<T> darCabeza(){
		return primero;
	}

	public void agregarElmentoInicio(T elem){
		if(primero == null){
			ultimo = new Nodo<T>(elem, null);
			primero = ultimo;
			referencia = primero;
			cantidad++;
		}else{
			Nodo<T> nuevo = new Nodo<T>(elem, primero);
			primero = nuevo;		
			cantidad++;
		}
	}

	public T eliminarElemento(){
		if(cantidad==0){
			throw new NoSuchElementException();}
		Nodo<T> temp = primero;
		primero = primero.getSiguiente();
		temp.setSiguiente(null);cantidad--;
		return (T) temp.getDato();
	}

	public T darElemento(int pos) {
		boolean res = false;
		T dat = null;
		if(primero !=null){
			int x = 0;
			Nodo<T> actual = primero;
			while(!res){
				if(x == pos){
					dat = actual.getDato();
					res =true;
				}
				else{
					actual = actual.getSiguiente();
					x++;
				}
			}
		}
		return dat;
	}

	public void cambiar(int pos,T dato){
		boolean res = false;		
		if(primero !=null){
			int x = 0;
			Nodo<T> actual = primero;
			while(!res){
				if(x == pos){
					actual.setDato(dato);
					res =true;
				}
				else{
					actual = actual.getSiguiente();
					x++;
				}
			}
		}

	}

	public int darNumeroElementos() {
		return cantidad;
	}

	public T darElementoPosicionActual() {
		return referencia.getDato();
	}

	public boolean avanzarSiguientePosicion()  {
		if(referencia.getSiguiente()==null){
			return false;
		}
		referencia=referencia.getSiguiente();
		return true;
	}

	public boolean retrocederPosicionAnterior() {
		boolean x = false;
		if(posicionReferencia-1 > 0){
			posicionReferencia--;
			x = true;
		}
		return x;
	}

	public void eliminarNodo (T elem)
	{
		if (primero.getDato().equals(elem))
			eliminarElemento();
		else
		{
			Nodo<T> actual = primero;
			boolean temp = false;
			while (!temp && actual.getSiguiente() != null)
			{
				if (actual.getSiguiente().getDato().equals(elem)){
					actual.setSiguiente(actual.getSiguiente().getSiguiente());
					temp = true;
				}
				actual = actual.getSiguiente();
			}
		}
	}

}
