package model.data_estructuras;

import java.util.Iterator;

public class ListaLlaveValorSecuencial<K,V> {

	private int cantidad;
	private ListaEncadenada<NodoTH<K,V>> noditos;
	private ListaEncadenada<K> kListo;

	public ListaLlaveValorSecuencial(){
		noditos = new ListaEncadenada<>();
		kListo = new ListaEncadenada<>();
		cantidad = 0;
	}

	public int darTamanio(){
		return cantidad;
	}

	public boolean estaVacia(){
		return cantidad == 0;
	}

	public boolean tieneLlave(K llave){
		boolean t = false;
		Iterator<NodoTH<K, V>> iter = noditos.iterator(); 
		while (iter.hasNext() && !t) {
			NodoTH<K, V> nodoTH = (NodoTH<K, V>) iter.next();
			t = nodoTH.getKey().equals(llave);
		}
		return t;
	}

	public V darValor(K llave){
		V rta = null;
		Iterator<NodoTH<K, V>> iter = noditos.iterator();
		while (iter.hasNext()) {
			NodoTH<K, V> nodoTH = (NodoTH<K, V>) iter.next();
			if(nodoTH.getKey().equals(llave)){
				rta = nodoTH.getValor();
			}
		}
		return rta;
	}

	public void insertar(K pLlave, V pValor){
		boolean t = tieneLlave(pLlave);
		if (! (!t && pValor == null))
		{
			if (!t && pValor != null)
			{
				NodoTH<K, V> n = new NodoTH<K, V>(pLlave, pValor);
				noditos.agregarElmentoInicio(n);
				kListo.agregarElmentoInicio(pLlave);
				cantidad++;
			}

			if (t && pValor != null)
			{
				Iterator<NodoTH<K, V>> iter = noditos.iterator();

				boolean en = false;
				while (iter.hasNext() && !en)
				{
					NodoTH<K, V> actual = iter.next();
					if (actual.getKey().equals(pLlave))
					{
						actual.change(pLlave, pValor);
						en = true;
					}
				}
			}
			
			else if (t && pValor == null)
			{
				eliminarNodo(pLlave);
				kListo.eliminarNodo(pLlave);
				cantidad--;
			}
		}
		
	}

	public Iterator<K> llaves(){
		return  kListo.iterator();
	}
	
	private void eliminarNodo (K pLlave)
	{
		Nodo<NodoTH<K, V>> cabeza = noditos.darCabeza();
		
		if (cabeza != null && cabeza.getDato().getKey().equals(pLlave))
			noditos.eliminarElemento();
		else if (cabeza != null)
		{
			Nodo<NodoTH<K, V>> act = cabeza;
			boolean e = false;
			
			while (!e && act.getSiguiente() != null)
			{
				if (act.getSiguiente().getDato().getKey().equals(pLlave))
				{
					act.setSiguiente(act.getSiguiente().getSiguiente());
					e = true;
				}
				act = act.getSiguiente();
			}
		}
	}
}
