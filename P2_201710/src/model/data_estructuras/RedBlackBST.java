package model.data_estructuras;

import java.util.Iterator;
import java.util.NoSuchElementException;



public class RedBlackBST<K extends Comparable<K>, V> {

	private static final boolean RED   = true;
	private static final boolean BLACK = false;

	private NodoArbol root;     

	private class NodoArbol {
		private K key;           
		private V val;         
		private NodoArbol left, right;  
		private boolean color;     
		private int size;         

		public NodoArbol(K key, V val, boolean color, int size) {
			this.key = key;
			this.val = val;
			this.color = color;
			this.size = size;
		}
	}

	public RedBlackBST() {
	}


	private boolean isRed(NodoArbol x) {
		if (x == null) return false;
		return x.color == RED;
	}

	private int size(NodoArbol x) {
		if (x == null) return 0;
		return x.size;
	} 


	public int size() {
		return size(root);
	}

	public boolean isEmpty() {
		return root == null;
	}


	public V get(K key) {
		if (key == null) throw new IllegalArgumentException("argument to get() is null");
		return get(root, key);
	}

	private V get(NodoArbol x, K key) {
		while (x != null) {
			int cmp = key.compareTo(x.key);
			if      (cmp < 0) x = x.left;
			else if (cmp > 0) x = x.right;
			else              return x.val;
		}
		return null;
	}

	public boolean contains(K key) {
		return get(key) != null;
	}

	public void put(K key, V val) {
		if (key == null) throw new IllegalArgumentException("first argument to put() is null");
		if (val == null) {
			delete(key);
			return;
		}

		root = put(root, key, val);
		root.color = BLACK;
	}

	private NodoArbol put(NodoArbol h, K key, V val) { 
		if (h == null) return new NodoArbol(key, val, RED, 1);

		int cmp = key.compareTo(h.key);
		if      (cmp < 0) h.left  = put(h.left,  key, val); 
		else if (cmp > 0) h.right = put(h.right, key, val); 
		else              h.val   = val;

		if (isRed(h.right) && !isRed(h.left))      h = rotateLeft(h);
		if (isRed(h.left)  &&  isRed(h.left.left)) h = rotateRight(h);
		if (isRed(h.left)  &&  isRed(h.right))     flipColors(h);
		h.size = size(h.left) + size(h.right) + 1;

		return h;
	}

	public void deleteMin() {
		if (isEmpty()) throw new NoSuchElementException("BST underflow");

		if (!isRed(root.left) && !isRed(root.right))
			root.color = RED;

		root = deleteMin(root);
		if (!isEmpty()) root.color = BLACK;
	}

	private NodoArbol deleteMin(NodoArbol h) { 
		if (h.left == null)
			return null;

		if (!isRed(h.left) && !isRed(h.left.left))
			h = moveRedLeft(h);

		h.left = deleteMin(h.left);
		return balance(h);
	}


	public void deleteMax() {
		if (isEmpty()) throw new NoSuchElementException("BST underflow");

		if (!isRed(root.left) && !isRed(root.right))
			root.color = RED;

		root = deleteMax(root);
		if (!isEmpty()) root.color = BLACK;
	}

	private NodoArbol deleteMax(NodoArbol h) { 
		if (isRed(h.left))
			h = rotateRight(h);

		if (h.right == null)
			return null;

		if (!isRed(h.right) && !isRed(h.right.left))
			h = moveRedRight(h);

		h.right = deleteMax(h.right);

		return balance(h);
	}

	public void delete(K key) { 
		if (key == null) throw new IllegalArgumentException("argument to delete() is null");
		if (!contains(key)) return;

		if (!isRed(root.left) && !isRed(root.right))
			root.color = RED;

		root = delete(root, key);
		if (!isEmpty()) root.color = BLACK;
	}

	private NodoArbol delete(NodoArbol h, K key) { 

		if (key.compareTo(h.key) < 0)  {
			if (!isRed(h.left) && !isRed(h.left.left))
				h = moveRedLeft(h);
			h.left = delete(h.left, key);
		}
		else {
			if (isRed(h.left))
				h = rotateRight(h);
			if (key.compareTo(h.key) == 0 && (h.right == null))
				return null;
			if (!isRed(h.right) && !isRed(h.right.left))
				h = moveRedRight(h);
			if (key.compareTo(h.key) == 0) {
				NodoArbol x = min(h.right);
				h.key = x.key;
				h.val = x.val;
				h.right = deleteMin(h.right);
			}
			else h.right = delete(h.right, key);
		}
		return balance(h);
	}
	
	public int height() {
        return height(root);
    }
    private int height(NodoArbol x) {
        if (x == null) return -1;
        return 1 + Math.max(height(x.left), height(x.right));
    }

	public K min() {
		if (isEmpty()) throw new NoSuchElementException("called min() with empty symbol table");
		return min(root).key;
	} 

	private NodoArbol min(NodoArbol x) { 
		if (x.left == null) return x; 
		else                return min(x.left); 
	} 

	public K max() {
		if (isEmpty()) throw new NoSuchElementException("called max() with empty symbol table");
		return max(root).key;
	} 

	private NodoArbol max(NodoArbol x) { 
		if (x.right == null) return x; 
		else                 return max(x.right); 
	} 

	private NodoArbol rotateRight(NodoArbol h) {
		NodoArbol x = h.left;
		h.left = x.right;
		x.right = h;
		x.color = x.right.color;
		x.right.color = RED;
		x.size = h.size;
		h.size = size(h.left) + size(h.right) + 1;
		return x;
	}

	private NodoArbol rotateLeft(NodoArbol h) {
		NodoArbol x = h.right;
		h.right = x.left;
		x.left = h;
		x.color = x.left.color;
		x.left.color = RED;
		x.size = h.size;
		h.size = size(h.left) + size(h.right) + 1;
		return x;
	}

	private void flipColors(NodoArbol h) {
		h.color = !h.color;
		h.left.color = !h.left.color;
		h.right.color = !h.right.color;
	}

	private NodoArbol moveRedLeft(NodoArbol h) {

		flipColors(h);
		if (isRed(h.right.left)) { 
			h.right = rotateRight(h.right);
			h = rotateLeft(h);
			flipColors(h);
		}
		return h;
	}

	private NodoArbol moveRedRight(NodoArbol h) {
		flipColors(h);
		if (isRed(h.left.left)) { 
			h = rotateRight(h);
			flipColors(h);
		}
		return h;
	}

	private NodoArbol balance(NodoArbol h) {

		if (isRed(h.right))                      h = rotateLeft(h);
		if (isRed(h.left) && isRed(h.left.left)) h = rotateRight(h);
		if (isRed(h.left) && isRed(h.right))     flipColors(h);

		h.size = size(h.left) + size(h.right) + 1;
		return h;
	}
	
	public boolean check() {        
        return (isBST() && isSizeConsistent() && is23() && isBalanced());
    }
	
	private boolean isBST() {
        return isBST(root, null, null);
    }

    private boolean isBST(NodoArbol x, K min, K max) {
        if (x == null) return true;
        if (min != null && x.key.compareTo(min) <= 0) return false;
        if (max != null && x.key.compareTo(max) >= 0) return false;
        return isBST(x.left, min, x.key) && isBST(x.right, x.key, max);
    } 

    private boolean isSizeConsistent() { return isSizeConsistent(root); }
    private boolean isSizeConsistent(NodoArbol x) {
        if (x == null) return true;
        if (x.size != size(x.left) + size(x.right) + 1) return false;
        return isSizeConsistent(x.left) && isSizeConsistent(x.right);
    } 
    
    public K select(int k) {
        if (k < 0 || k >= size()) {
            throw new IllegalArgumentException("called select() with invalid argument: " + k);
        }
        NodoArbol x = select(root, k);
        return x.key;
    }
    
    private NodoArbol select(NodoArbol x, int k) {
        int t = size(x.left); 
        if      (t > k) return select(x.left,  k); 
        else if (t < k) return select(x.right, k-t-1); 
        else            return x; 
    }
    
    public int rank(K key) {
        if (key == null) throw new IllegalArgumentException("argument to rank() is null");
        return rank(key, root);
    } 

    private int rank(K key, NodoArbol x) {
        if (x == null) return 0; 
        int cmp = key.compareTo(x.key); 
        if      (cmp < 0) return rank(key, x.left); 
        else if (cmp > 0) return 1 + size(x.left) + rank(key, x.right); 
        else              return size(x.left); 
    } 

    private boolean is23() { return is23(root); }
    private boolean is23(NodoArbol x) {
        if (x == null) return true;
        if (isRed(x.right)) return false;
        if (x != root && isRed(x) && isRed(x.left))
            return false;
        return is23(x.left) && is23(x.right);
    } 

    private boolean isBalanced() { 
        int black = 0;     
        NodoArbol x = root;
        while (x != null) {
            if (!isRed(x)) black++;
            x = x.left;
        }
        return isBalanced(root, black);
    }

    private boolean isBalanced(NodoArbol x, int black) {
        if (x == null) return black == 0;
        if (!isRed(x)) black--;
        return isBalanced(x.left, black) && isBalanced(x.right, black);
    } 
    
    public Iterator<K> keys() 
	{
		// TODO Auto-generated method stub
		if(isEmpty()) return new ListaEncadenada().iterator();
		return keys(min(),max());
	}

	private Iterator<K> keys(K min, K max) 
	{
		if (min == null || max== null) throw new IllegalArgumentException("El argumento es nulo");

		ListaEncadenada<K> lista = new ListaEncadenada<K>();
		keys(root,lista, min, max);
		return lista.iterator();
	}
	
	public ListaEncadenada<V> inOrden(){
		ListaEncadenada<V>  rpta = new ListaEncadenada<>();
		Iterator<K> iter = this.keys();
		while(iter.hasNext()){
			rpta.agregarElementoFinal(this.get(iter.next()));
		}
		return rpta;
	}
    
    private void keys(NodoArbol x, ListaEncadenada<K> listilla, K lo, K hi) { 
        if (x == null) return; 
        int cmplo = lo.compareTo(x.key); 
        int cmphi = hi.compareTo(x.key); 
        if (cmplo < 0) keys(x.left, listilla, lo, hi); 
        if (cmplo <= 0 && cmphi >= 0) listilla.agregarElementoFinal(x.key); 
        if (cmphi > 0) keys(x.right, listilla, lo, hi); 
    } 
}
