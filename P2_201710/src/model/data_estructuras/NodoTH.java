package model.data_estructuras;

public class NodoTH<K,V> {

	private V valor;
	private K key;


	public NodoTH( K pKey, V pDato){
		valor = pDato;
		key = pKey;
	}

	
	public V getValor() {
		return valor;
	}


	public void setValor(V valor) {
		this.valor = valor;
	}
	
	public K getKey() {
		return key;
	}

	public void setKey(K key) {
		this.key = key;
	}
	
	public void change(K key, V valor ){
		this.valor = valor;
		this.key = key;
	}

}
