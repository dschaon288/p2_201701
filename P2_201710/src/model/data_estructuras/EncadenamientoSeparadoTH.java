package model.data_estructuras;

import java.util.Iterator;

public class EncadenamientoSeparadoTH<K,V> {

	private int tamanho;

	private ListaLlaveValorSecuencial<K, V> [] lista;

	private ListaEncadenada<K> kListo;

	public EncadenamientoSeparadoTH(int m){
		tamanho = 0;
		kListo = new ListaEncadenada<K>();
		lista = new ListaLlaveValorSecuencial[m];
		for (int i = 0; i < lista.length; i++) {
			lista[i] = new ListaLlaveValorSecuencial<K,V>();
		}
	}

	public int darTamanio(){
		return tamanho;
	}

	public boolean estaVacia(){
		return tamanho == 0;
	}

	public boolean tieneLlave(K llave){
		int h = hash(llave);
		return lista[h].tieneLlave(llave);
	}

	public V darValor(K llave){
		int h = hash(llave);
		return lista[h].darValor(llave);
	}

	public void  insertar(K llave, V valor){
		boolean t = tieneLlave(llave);
		
		if(! (!t && valor == null) ){
			int c = hash(llave);
			if(t && valor == null){
				lista[c].insertar(llave, valor);
				kListo.eliminarNodo(llave);
				tamanho--;
			}
			else if(!t && valor !=null ){
				lista[c].insertar(llave, valor);
				kListo.agregarElmentoInicio(llave);
				tamanho++;
			}
			else if(t && valor != null){
				lista[c].insertar(llave, valor);
			}
			rehashTable();
		}
	}

	public Iterator<K> llaves(){
		ListaEncadenada<K> rpta = new ListaEncadenada<>();
		for(ListaLlaveValorSecuencial<K, V> zz : lista){
			Iterator<K> iter = zz.llaves();

			while(iter.hasNext()){
				rpta.agregarElementoFinal(iter.next());
			}
		}

		return rpta.iterator();
	} 

	public int hash(K llave){
		return (llave.hashCode() & 0x7fffffff) % lista.length;
	}
	
	public void rehashTable ()
	{
		
		double factorDeCarga = tamanho/lista.length;
			if (factorDeCarga >= 8)
			{
				resize(2*lista.length);
				
			}
			else if (factorDeCarga <= 2 && lista.length > 2)
			{
				resize(lista.length/2);
			}
	}
	
	
	private void insertarRH(K llave, V valor) {

		boolean tieneLlave = tieneLlave(llave);

		if (! (!tieneLlave && valor == null) )
		{
			int codigo = hash(llave);
			if (!tieneLlave && valor != null)
			{
				lista[codigo].insertar(llave, valor);
				kListo.agregarElmentoInicio(llave);
				tamanho++;
			}
			else if (tieneLlave && valor == null)
			{
				lista[codigo].insertar(llave, valor);
				kListo.eliminarNodo(llave);
				tamanho--;
			}
			else if (tieneLlave && valor != null)
				lista[codigo].insertar(llave, valor);	
		}
	}
	
	private void resize(int n){
		Iterator <K> iterLlaves = (Iterator<K>) llaves();

		EncadenamientoSeparadoTH<K, V> nTH = new EncadenamientoSeparadoTH<K, V>(n);

		while (iterLlaves.hasNext())
		{
			K llaveActual = iterLlaves.next();
			V valorActual = darValor(llaveActual);

			nTH.insertarRH(llaveActual, valorActual);
		}
		lista = nTH.lista;	
	}

	public int[] darLongitudListas(){

		int[] lLista= new int[lista.length];

		for (int i = 0; i< lista.length; i++)
		{
			lLista[i] = lista[i].darTamanio();
		}

		return lLista;
	}
}
