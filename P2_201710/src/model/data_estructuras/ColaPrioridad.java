package model.data_estructuras;

import java.util.Comparator;

public class ColaPrioridad<T extends Comparable<T>> {

	private T[] cola;
	private int max;
	private int size;

	public ColaPrioridad(int max) {

		cola= (T[]) new Comparable[max];
		this.max = max;
		size = 0;
	}

	public int darNumeroElementos(){
		return size;
	}

	public void agregar(T elemento, Comparator<T> c) throws Exception{

		if(size < max)
		{
			size++;
			
			int p = size-1;
			
			cola[p]=elemento;
			while(p>0&& !less( cola[p],cola[p-1], c)){
				exchange(p-1,p);
				p--;
			}
		}
		else
		{
			throw new Exception("Se ha llenado la cola");
		}
	}

	private void exchange(int p, int i) {
		T t = cola[p];
		cola[p]=cola[i];
		cola[i]=t;
		
	}

	private boolean less(T t, T t2, Comparator<T> c) {
		
		int compare = 0;
		compare = c.compare(t, t2);
		return compare < 0;
	}

	public T max(){
		T rpta = null;
		if(size == 0)
		{
			return null;
		}
		else
		{
			rpta = cola[0];
			eliminarPrimerElemento();
		}
		return rpta;
	}

	public boolean isEmpty(){

		return size == 0;
	}

	public int tamanoMax(){

		return max;
	}
	
	public void eliminarPrimerElemento(){
		T[] temp = (T[]) new Comparable[max];
		for(int i=1; i<cola.length;i++)
		{
			temp[i-1]= cola[i];
		}
		cola=temp;
		size--;
	}
	
	

}