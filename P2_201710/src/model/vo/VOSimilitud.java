package model.vo;

public class VOSimilitud {
	 
	private double simil;
	
	private Long idComparador;

	public Long getIdComparador() {
		return idComparador;
	}

	public void setIdComparador(Long idComparador) {
		this.idComparador = idComparador;
	}

	public double getSimil() {
		return simil;
	}

	public void setSimil(double simil) {
		this.simil = simil;
	}
}
