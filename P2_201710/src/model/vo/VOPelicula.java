package model.vo;


import java.util.Date;

import api.ILista;

public class VOPelicula implements Comparable<VOPelicula>{
	/*
	 * nombre de la pel�cula
	 */
	private String nombre;
	
	/*
	 * Fecha de lanzamiento de la pel�cula
	 */
	private Date fechaLanzamineto;
	
	/*
	 * Lista con los generos asociados a la pel�cula
	 */
	
	private ILista<VOGeneroPelicula> generosAsociados;
	
	/*
	 * votos totales sobre la pel�cula
	 */
	 private int votostotales;  
	 
	 /*
	  * promedio anual de votos (hasta el 2016)
	  */
	 private double promedioAnualVotos; 
	 
	 /*
	  * promedio IMBD
	  */
	 
	 private double ratingIMBD;	
	 
	 private int year;
	 
	 private String paisito;
	 
	 public VOPelicula() {
		// TODO Auto-generated constructor stub
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Date getFechaLanzamineto() {
		return fechaLanzamineto;
	}

	public void setFechaLanzamineto(Date fechaLanzamineto) {
		this.fechaLanzamineto = fechaLanzamineto;
	}

	public ILista<VOGeneroPelicula> getGenerosAsociados() {
		return generosAsociados;
	}

	public void setGenerosAsociados(ILista<VOGeneroPelicula> generosAsociados) {
		this.generosAsociados = generosAsociados;
	}

	public int getVotostotales() {
		return votostotales;
	}

	public void setVotostotales(int votostotales) {
		this.votostotales = votostotales;
	}

	public double getPromedioAnualVotos() {
		return promedioAnualVotos;
	}

	public void setPromedioAnualVotos(double promedioAnualVotos) {
		this.promedioAnualVotos = promedioAnualVotos;
	}

	public double getRatingIMBD() {
		return ratingIMBD;
	}

	public void setRatingIMBD(double ratingIMBD) {
		this.ratingIMBD = ratingIMBD;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	@Override
	public int compareTo(VOPelicula o) {
		if(promedioAnualVotos*ratingIMBD == o.getPromedioAnualVotos()*o.getRatingIMBD())
			return 0;
		else if(promedioAnualVotos*ratingIMBD < o.getPromedioAnualVotos()*o.getRatingIMBD())
			return -1;
		else
			return 1;
		
	}

	public String getPaisito() {
		return paisito;
	}

	public void setPaisito(String paisito) {
		this.paisito = paisito;
	}
	 
	 
	
}
