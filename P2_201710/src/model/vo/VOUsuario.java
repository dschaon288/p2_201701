package model.vo;

import java.util.Date;

import api.ILista;

public class VOUsuario {
	
	private int idUsuario;
	
	private ILista<VOUsuarioPelicula> nose;
	
	private Date timeS ;
	
	private VOUsuarioEntrada entrada;

	public int getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(int idUsuario) {
		this.idUsuario = idUsuario;
	}

	public ILista<VOUsuarioPelicula> getNose() {
		return nose;
	}

	public void setNose(ILista<VOUsuarioPelicula> nose) {
		this.nose = nose;
	}

	public Date getTimeS() {
		return timeS;
	}

	public void setTimeS(Date timeS) {
		this.timeS = timeS;
	}

	public VOUsuarioEntrada getEntrada() {
		return entrada;
	}

	public void setEntrada(VOUsuarioEntrada entrada) {
		this.entrada = entrada;
	}

}
