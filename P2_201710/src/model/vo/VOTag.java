package model.vo;

public class VOTag {
	
	/**
	 * contenido del tag
	 */
	private String contenido;
	
	private Integer idUsuarios;
	
	private Long idPelicula;
	
	public VOTag() {
		// TODO Auto-generated constructor stub
	}
	
	public String getContenido() {
		return contenido;
	}
	public void setContenido(String contenido) {
		this.contenido = contenido;
	}

	public Integer getIdUsuarios() {
		return idUsuarios;
	}

	public void setIdUsuarios(Integer id) {
		this.idUsuarios = id;
	}

	public Long getIdPelicula() {
		return idPelicula;
	}

	public void setIdPelicula(Long idPelicula) {
		this.idPelicula = idPelicula;
	}
	
}
