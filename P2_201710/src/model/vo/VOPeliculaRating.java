package model.vo;

public class VOPeliculaRating implements Comparable<VOPeliculaRating>{
	
	private Long id;
	
	private Number rating;

	public Number getRating() {
		return rating;
	}

	public void setRating(Number rating) {
		this.rating = rating;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public int compareTo(VOPeliculaRating o) {
		if(rating.doubleValue() < o.getRating().doubleValue())
			return -1;
		else if(rating.doubleValue() == o.getRating().doubleValue() )
			return 0;
		else
			return 1;
	}
}
