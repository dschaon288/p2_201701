package model.vo;

import java.util.Date;

import api.ILista;

/**
 * @author Venegas
 *
 */

public class VOUsuarioPelicula {
	
	/**
	 * nombre de la pel�cula
	 */

	private String nombrePelicula;
	
	private Long idPeliculon;
	
	/**
	 * rating dado por el usuario
	 */
	private double ratingUsuario;
	
	/**
	 * rating calculado por el sistema
	 */

	private double ratingSistema;
	
	/**
	 * error sobre el rating
	 */
	
	private double errorRating;
	
	/**
	 * list de tags generados por el usuario sobre la pelicula
	 */
	
	private ILista<VOTag> tags;
	
	/**
	 * id del usuario
	 */
	
	private Integer idUsuario;
	
	private Date fPrimerR;


	public VOUsuarioPelicula() {
		// TODO Auto-generated constructor stub
	}
	
	public String getNombrepelicula() {
		return nombrePelicula;
	}

	public void setNombrepelicula(String nombrepelicula) {
		this.nombrePelicula = nombrepelicula;
	}

	public double getRatingUsuario() {
		return ratingUsuario;
	}

	public void setRatingUsuario(double ratingUsuario) {
		this.ratingUsuario = ratingUsuario;
	}

	public double getRatingSistema() {
		return ratingSistema;
	}

	public void setRatingSistema(double ratingSistema) {
		this.ratingSistema = ratingSistema;
	}

	public double getErrorRating() {
		return errorRating;
	}

	public void setErrorRating(double errorRating) {
		this.errorRating = errorRating;
	}

	public ILista<VOTag> getTags() {
		return tags;
	}

	public void setTags(ILista<VOTag> tags) {
		this.tags = tags;
	}

	public Integer getIdUsuario() {
		return idUsuario;
	}

	public void setIdUsuario(Integer idUsuario) {
		this.idUsuario = idUsuario;
	}

	public Date getfPrimerR() {
		return fPrimerR;
	}

	public void setfPrimerR(Date fPrimerR) {
		this.fPrimerR = fPrimerR;
	}

	public Long getIdPeliculon() {
		return idPeliculon;
	}

	public void setIdPeliculon(Long idPeliculon) {
		this.idPeliculon = idPeliculon;
	}
}
