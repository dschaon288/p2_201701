package model.vo;

import api.ILista;

public class VOGeneroPelicula implements Comparable<VOGeneroPelicula>{

	/*
	 * nombre del genero
	 */
	
	private String nombre;
	
	public VOGeneroPelicula() {
		// TODO Auto-generated constructor stub
	}
	
	public String getNombre() {
		return nombre;
	}
	
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	@Override
	public int compareTo(VOGeneroPelicula o) {
		
		if(nombre.compareToIgnoreCase(o.getNombre())<0)
		{
			return -1;
		}
		else if(nombre.compareToIgnoreCase(o.getNombre())>0)
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}
	
}
