package model.vo;


import api.ILista;
import model.data_estructuras.*;

public class VOUsuarioEntrada implements Comparable<VOUsuarioEntrada>{

	private String usuarioName;

	private VOUsuario usu;

	private ILista<VOPeliculaRating> peliculitasDadas;

	private ColaPrioridad<VOPeliculaRating> peliculitasRecomendadas;

	public String getUsuarioName() {
		return usuarioName;
	}

	public void setUsuarioName(String usuario) {
		this.usuarioName = usuario;
	}

	public VOUsuario getUsu() {
		return usu;
	}

	public void setUsu(VOUsuario usu) {
		this.usu = usu;
	}

	public boolean esUsuario(){
		if(usu !=null)
			return true;
		else
			return false;
	}

	public ILista<VOPeliculaRating> getPeliculitasDadas() {
		return peliculitasDadas;
	}

	public void setPeliculitasDadas(ILista<VOPeliculaRating> peliculitasDadas) {
		this.peliculitasDadas = peliculitasDadas;
	}

	public int compareTo(VOUsuarioEntrada o) {
		if(usuarioName !=null){
			return -1;
		}
		else if (usu !=null){
			if(usu.getTimeS().before( o.getUsu().getTimeS())){
				return 1;
			}
			else  if(usu.getTimeS().equals(o.getUsu().getTimeS())){
				if(usu.getNose().darNumeroElementos() > o.getUsu().getNose().darNumeroElementos()){
					return 1;
				}
				else if(usu.getNose().darNumeroElementos() == o.getUsu().getNose().darNumeroElementos()){
					return (usu.getIdUsuario() - o.getUsu().getIdUsuario());
				}
				else
					return -1;
			}
			else
				return -1;

		}
		else
			return -1;
	}

	public ColaPrioridad<VOPeliculaRating> getPeliculitasRecomendadas() {
		return peliculitasRecomendadas;
	}

	public void setPeliculitasRecomendadas(ColaPrioridad<VOPeliculaRating> peliculitasRecomendadas) {
		this.peliculitasRecomendadas = peliculitasRecomendadas;
	}
}
